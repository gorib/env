package env

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNotExists(t *testing.T) {
	t.Run("Value", func(t *testing.T) {
		var err error
		_, err = MustValue[string]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustValue[int]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustValue[float64]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustValue[bool]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)

		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedValue[string]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedValue[int]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedValue[float64]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedValue[bool]("TEST_NOT_EXISTS") })

		assert.Equal(t, "just a string", Value("TEST_NOT_EXISTS", "just a string"))
		assert.Equal(t, 100, Value("TEST_NOT_EXISTS", 100))
		assert.Equal(t, 100.0, Value("TEST_NOT_EXISTS", 100.0))
		assert.Equal(t, true, Value("TEST_NOT_EXISTS", true))
		assert.Equal(t, false, Value("TEST_NOT_EXISTS", false))
	})
	t.Run("Array", func(t *testing.T) {
		var err error
		_, err = MustArray[string]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustArray[int]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustArray[float64]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)
		_, err = MustArray[bool]("TEST_NOT_EXISTS")
		assert.ErrorIs(t, err, ErrNoEnvFound)

		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedArray[string]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedArray[int]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedArray[float64]("TEST_NOT_EXISTS") })
		assert.PanicsWithError(t, "no env found: TEST_NOT_EXISTS", func() { _ = NeedArray[bool]("TEST_NOT_EXISTS") })

		assert.Equal(t, []string{"just", "a", "string"}, Array("TEST_NOT_EXISTS", []string{"just", "a", "string"}))
		assert.Equal(t, []int{100, 200, 300}, Array("TEST_NOT_EXISTS", []int{100, 200, 300}))
		assert.Equal(t, []float64{100.0, 200.0, 300.0}, Array("TEST_NOT_EXISTS", []float64{100, 200, 300}))
		assert.Equal(t, []bool{true, false, true}, Array("TEST_NOT_EXISTS", []bool{true, false, true}))
	})
	t.Run("Json", func(t *testing.T) {
		// No env variable set. Should fallback to provided default value.
		result := Json("TEST_JSON_NOT_EXISTS", "default string")
		assert.Equal(t, "default string", result)

		// Test custom type
		type CustomStruct struct {
			Field1 string
			Field2 int
		}

		expected := CustomStruct{"default", 10}
		resultStruct := Json("TEST_CUSTOM_JSON_NOT_EXISTS", expected)
		assert.Equal(t, expected, resultStruct)

		assert.PanicsWithError(t, "no env found: TEST_CUSTOM_JSON_NOT_EXISTS", func() { _ = NeedJson[map[string][]string]("TEST_CUSTOM_JSON_NOT_EXISTS") })
	})
}

func TestEmpty(t *testing.T) {
	_ = os.Setenv("TEST_EMPTY", "")

	t.Run("Value", func(t *testing.T) {
		value, err := MustValue[string]("TEST_EMPTY")
		assert.Nil(t, err)
		assert.Equal(t, "", value)

		_, err = MustValue[int]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		_, err = MustValue[float64]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		_, err = MustValue[bool]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		assert.Equal(t, "", NeedValue[string]("TEST_EMPTY"))
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: strconv.ParseInt: parsing "": invalid syntax`,
			func() { _ = NeedValue[int]("TEST_EMPTY") },
		)
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: strconv.ParseFloat: parsing "": invalid syntax`,
			func() { _ = NeedValue[float64]("TEST_EMPTY") },
		)
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: non-bool expression: ''`,
			func() { _ = NeedValue[bool]("TEST_EMPTY") },
		)

		assert.Equal(t, "", Value("TEST_EMPTY", "just a string"))
		assert.Equal(t, 100, Value("TEST_EMPTY", 100))
		assert.Equal(t, 100.0, Value("TEST_EMPTY", 100.0))
		assert.Equal(t, true, Value("TEST_EMPTY", true))
	})
	t.Run("Array", func(t *testing.T) {
		value, err := MustArray[string]("TEST_EMPTY")
		assert.Nil(t, err)
		assert.Equal(t, []string{""}, value)

		_, err = MustArray[int]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		_, err = MustArray[float64]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		_, err = MustArray[bool]("TEST_EMPTY")
		assert.ErrorIs(t, err, ErrWrongFormat)

		assert.Equal(t, []string{""}, NeedArray[string]("TEST_EMPTY"))
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: strconv.ParseInt: parsing "": invalid syntax`,
			func() { _ = NeedArray[int]("TEST_EMPTY") },
		)
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: strconv.ParseFloat: parsing "": invalid syntax`,
			func() { _ = NeedArray[float64]("TEST_EMPTY") },
		)
		assert.PanicsWithError(
			t,
			`error during parse environment: TEST_EMPTY: non-bool expression: ''`,
			func() { _ = NeedArray[bool]("TEST_EMPTY") },
		)

		assert.Equal(t, []string{""}, Array("TEST_EMPTY", []string{"just", "a", "string"}))
		assert.Equal(t, []int{100, 200, 300}, Array("TEST_EMPTY", []int{100, 200, 300}))
		assert.Equal(t, []float64{100.0, 200.0, 300.0}, Array("TEST_EMPTY", []float64{100, 200, 300}))
		assert.Equal(t, []bool{true, false, true}, Array("TEST_EMPTY", []bool{true, false, true}))
	})
	t.Run("Json", func(t *testing.T) {
		var err error

		// Should return an error, because an empty string cannot be unmarshalled into the expected type.
		_, err = MustJson[string]("TEST_EMPTY")
		assert.Error(t, err)
		assert.ErrorIs(t, err, ErrWrongFormat)

		type CustomStruct struct {
			Field1 string
			Field2 int
		}

		_, err = MustJson[CustomStruct]("TEST_EMPTY")
		assert.Error(t, err)
		assert.ErrorIs(t, err, ErrWrongFormat)
	})
}

func TestString(t *testing.T) {
	_ = os.Setenv("TEST_STRING", "test")
	_ = os.Setenv("TEST_ARRAY_ONE_ELEMENT", "1.0.0")
	_ = os.Setenv("TEST_ARRAY", "one,two,three")
	_ = os.Setenv("TEST_JSON", `"test string"`)
	_ = os.Setenv("TEST_CUSTOM_JSON", `{"Field1": "value1", "Field2": 2}`)

	t.Run("Value", func(t *testing.T) {
		value, err := MustValue[string]("TEST_STRING")
		assert.Nil(t, err)
		assert.Equal(t, "test", value)

		assert.Equal(t, "test", NeedValue[string]("TEST_STRING"))

		assert.Equal(t, "test", Value("TEST_STRING", "just a string"))
	})
	t.Run("Array", func(t *testing.T) {
		value, err := MustValue[string]("TEST_ARRAY_ONE_ELEMENT")
		assert.Nil(t, err)
		array, err := MustArray[string]("TEST_ARRAY_ONE_ELEMENT")
		assert.Nil(t, err)
		assert.Equal(t, []string{value}, array)

		assert.Equal(t, []string{NeedValue[string]("TEST_ARRAY_ONE_ELEMENT")}, NeedArray[string]("TEST_ARRAY_ONE_ELEMENT"))
		assert.Equal(t, []string{"one", "two", "three"}, NeedArray[string]("TEST_ARRAY"))

		array, err = MustArray[string]("TEST_ARRAY")
		assert.Nil(t, err)
		assert.Equal(t, []string{"one", "two", "three"}, array)

		assert.Equal(t, []string{NeedValue[string]("TEST_ARRAY_ONE_ELEMENT")}, Array("TEST_ARRAY_ONE_ELEMENT", []string{"default", "array"}))
		assert.Equal(t, []string{"one", "two", "three"}, Array("TEST_ARRAY", []string{"default", "array"}))
	})
	t.Run("Json", func(t *testing.T) {
		var err error

		value, err := MustJson[string]("TEST_JSON")
		assert.Nil(t, err)
		assert.Equal(t, "test string", value)

		assert.Equal(t, "test string", NeedJson[string]("TEST_JSON"))
		assert.Equal(t, "test string", Json("TEST_JSON", "default string"))

		type CustomStruct struct {
			Field1 string
			Field2 int
		}

		expected := CustomStruct{"value1", 2}
		valueStruct, err := MustJson[CustomStruct]("TEST_CUSTOM_JSON")
		assert.Nil(t, err)
		assert.Equal(t, expected, valueStruct)
		assert.Equal(t, expected, NeedJson[CustomStruct]("TEST_CUSTOM_JSON"))
		assert.Equal(t, expected, Json("TEST_CUSTOM_JSON", CustomStruct{"default", 10}))
	})
}

func TestInt(t *testing.T) {
	_ = os.Setenv("TEST_STRING", "1.0")
	_ = os.Setenv("TEST_INT", "1000000")
	_ = os.Setenv("TEST_UINT", "-1")
	_ = os.Setenv("TEST_ARRAY_ONE_ELEMENT", "1")
	_ = os.Setenv("TEST_ARRAY", "1 ,2, 3")
	_ = os.Setenv("TEST_ARRAY_NOT_INT", "1,2d,3")
	_ = os.Setenv("TEST_TIME_DURATION", "30s")

	t.Run(
		"Value",
		func(t *testing.T) {
			_, err := MustValue[int]("TEST_STRING")
			assert.ErrorIs(t, err, ErrWrongFormat)

			value, err := MustValue[int]("TEST_INT")
			assert.Nil(t, err)
			assert.Equal(t, 1000000, value)

			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: strconv.ParseInt: parsing "1.0": invalid syntax`,
				func() { _ = NeedValue[int]("TEST_STRING") },
			)
			assert.Equal(t, 1000000, NeedValue[int]("TEST_INT"))

			assert.Equal(t, 100, Value("TEST_STRING", 100))
			assert.Equal(t, 1000000, Value("TEST_INT", 100))
			assert.Equal(t, int64(1000000), Value("TEST_INT", int64(100)))
			assert.Equal(t, int32(1000000), Value("TEST_INT", int32(100)))
			assert.Equal(t, int16(100), Value("TEST_INT", int16(100)))
			assert.Equal(t, int8(100), Value("TEST_INT", int8(100)))
			assert.Equal(t, uint(1000000), Value("TEST_INT", uint(100)))
			assert.Equal(t, uint64(1000000), Value("TEST_INT", uint64(100)))
			assert.Equal(t, uint32(1000000), Value("TEST_INT", uint32(100)))
			assert.Equal(t, uint16(100), Value("TEST_INT", uint16(100)))
			assert.Equal(t, uint8(100), Value("TEST_INT", uint8(100)))
			assert.Equal(t, uint(100), Value("TEST_UINT", uint(100)))
			assert.Equal(t, 30*time.Second, Value("TEST_TIME_DURATION", 20*time.Second))
		},
	)

	t.Run(
		"Array",
		func(t *testing.T) {
			_, err := MustArray[int]("TEST_STRING")
			assert.ErrorIs(t, err, ErrWrongFormat)

			_, err = MustArray[int]("TEST_ARRAY_NOT_INT")
			assert.ErrorIs(t, err, ErrWrongFormat)

			value, err := MustValue[int]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			array, err := MustArray[int]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			assert.Equal(t, []int{value}, array)

			array, err = MustArray[int]("TEST_ARRAY")
			assert.Nil(t, err)
			assert.Equal(t, []int{1, 2, 3}, array)

			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: strconv.ParseInt: parsing "1.0": invalid syntax`,
				func() { _ = NeedArray[int]("TEST_STRING") },
			)
			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_ARRAY_NOT_INT: strconv.ParseInt: parsing "2d": invalid syntax`,
				func() { _ = NeedArray[int]("TEST_ARRAY_NOT_INT") },
			)
			assert.Equal(t, []int{NeedValue[int]("TEST_ARRAY_ONE_ELEMENT")}, NeedArray[int]("TEST_ARRAY_ONE_ELEMENT"))
			assert.Equal(t, []int{1, 2, 3}, NeedArray[int]("TEST_ARRAY"))

			assert.Equal(t, []int{100, 200, 300}, Array("TEST_STRING", []int{100, 200, 300}))
			assert.Equal(t, []int{100, 200, 300}, Array("TEST_ARRAY_NOT_INT", []int{100, 200, 300}))
			assert.Equal(t, []int{value}, Array("TEST_ARRAY_ONE_ELEMENT", []int{1000, 2000}))
			assert.Equal(t, []int{1, 2, 3}, Array("TEST_ARRAY", []int{100, 200, 300}))
		},
	)
}

func TestFloat(t *testing.T) {
	_ = os.Setenv("TEST_STRING", "1.0.0")
	_ = os.Setenv("TEST_FLOAT", "1")
	_ = os.Setenv("TEST_ARRAY_ONE_ELEMENT", "1.3")
	_ = os.Setenv("TEST_ARRAY", "1 ,2, 3")
	_ = os.Setenv("TEST_ARRAY_NOT_FLOAT", "1,2d,3")

	t.Run(
		"Value",
		func(t *testing.T) {
			_, err := MustArray[float64]("TEST_STRING")
			assert.ErrorIs(t, err, ErrWrongFormat)

			_, err = MustArray[float64]("TEST_ARRAY_NOT_FLOAT")
			assert.ErrorIs(t, err, ErrWrongFormat)

			value, err := MustValue[float64]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			array, err := MustArray[float64]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			assert.Equal(t, []float64{value}, array)

			array, err = MustArray[float64]("TEST_ARRAY")
			assert.Nil(t, err)
			assert.Equal(t, []float64{1, 2, 3}, array)

			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: strconv.ParseFloat: parsing "1.0.0": invalid syntax`,
				func() { _ = NeedValue[float64]("TEST_STRING") },
			)
			assert.Equal(t, 1.0, NeedValue[float64]("TEST_FLOAT"))

			assert.Equal(t, 100.0, Value("TEST_STRING", 100.0))
			assert.Equal(t, 1.0, Value("TEST_FLOAT", 100.0))
			assert.Equal(t, float32(1), Value("TEST_FLOAT", float32(100.0)))
		},
	)

	t.Run(
		"Array",
		func(t *testing.T) {
			_, err := MustArray[float64]("TEST_STRING")
			assert.ErrorIs(t, err, ErrWrongFormat)

			_, err = MustArray[float64]("TEST_ARRAY_NOT_FLOAT")
			assert.ErrorIs(t, err, ErrWrongFormat)

			value, err := MustValue[float64]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			array, err := MustArray[float64]("TEST_ARRAY_ONE_ELEMENT")
			assert.Nil(t, err)
			assert.Equal(t, []float64{value}, array)

			array, err = MustArray[float64]("TEST_ARRAY")
			assert.Nil(t, err)
			assert.Equal(t, []float64{1, 2, 3}, array)

			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: strconv.ParseFloat: parsing "1.0.0": invalid syntax`,
				func() { _ = NeedArray[float64]("TEST_STRING") },
			)
			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_ARRAY_NOT_FLOAT: strconv.ParseFloat: parsing "2d": invalid syntax`,
				func() { _ = NeedArray[float64]("TEST_ARRAY_NOT_FLOAT") },
			)
			assert.Equal(t, []float64{NeedValue[float64]("TEST_ARRAY_ONE_ELEMENT")}, NeedArray[float64]("TEST_ARRAY_ONE_ELEMENT"))
			assert.Equal(t, []float64{1, 2, 3}, NeedArray[float64]("TEST_ARRAY"))

			assert.Equal(t, []float64{100, 200, 300}, Array("TEST_STRING", []float64{100, 200, 300}))
			assert.Equal(t, []float64{100, 200, 300}, Array("TEST_ARRAY_NOT_INT", []float64{100, 200, 300}))
			assert.Equal(t, []float64{NeedValue[float64]("TEST_ARRAY_ONE_ELEMENT")}, Array("TEST_ARRAY_ONE_ELEMENT", []float64{1000, 2000}))
			assert.Equal(t, []float64{1, 2, 3}, Array("TEST_ARRAY", []float64{100, 200, 300}))
		},
	)
}

func TestBool(t *testing.T) {
	_ = os.Setenv("TEST_STRING", "1.0.0")
	_ = os.Setenv("TEST_BOOL_FALSE", "false")
	_ = os.Setenv("TEST_BOOL_TRUE", "true")
	_ = os.Setenv("TEST_BOOL_ONE", "1")
	_ = os.Setenv("TEST_ARRAY_ONE_ELEMENT", "true")
	_ = os.Setenv("TEST_ARRAY", "1, false ,true, 0")
	_ = os.Setenv("TEST_ARRAY_NOT_BOOL", "1,true,3")

	t.Run(
		"Value",
		func(t *testing.T) {
			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: non-bool expression: '1.0.0'`,
				func() { _ = NeedValue[bool]("TEST_STRING") },
			)
			assert.Equal(t, false, NeedValue[bool]("TEST_BOOL_FALSE"))
			assert.Equal(t, true, NeedValue[bool]("TEST_BOOL_TRUE"))
			assert.Equal(t, true, NeedValue[bool]("TEST_BOOL_ONE"))

			assert.Equal(t, true, Value("TEST_EMPTY", true))
			assert.Equal(t, false, Value("TEST_STRING", false))
			assert.Equal(t, false, Value("TEST_BOOL_FALSE", true))
			assert.Equal(t, true, Value("TEST_BOOL_TRUE", false))
			assert.Equal(t, true, Value("TEST_BOOL_ONE", false))
		},
	)

	t.Run(
		"Array",
		func(t *testing.T) {
			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_STRING: non-bool expression: '1.0.0'`,
				func() { _ = NeedArray[bool]("TEST_STRING") },
			)
			assert.PanicsWithError(
				t,
				`error during parse environment: TEST_ARRAY_NOT_BOOL: non-bool expression: '3'`,
				func() { _ = NeedArray[bool]("TEST_ARRAY_NOT_BOOL") },
			)
			assert.Equal(t, []bool{NeedValue[bool]("TEST_ARRAY_ONE_ELEMENT")}, NeedArray[bool]("TEST_ARRAY_ONE_ELEMENT"))
			assert.Equal(t, []bool{true, false, true, false}, NeedArray[bool]("TEST_ARRAY"))

			assert.Equal(t, []bool{true, false}, Array("TEST_STRING", []bool{true, false}))
			assert.Equal(t, []bool{false, true, false}, Array("TEST_ARRAY_NOT_BOOL", []bool{false, true, false}))
			assert.Equal(t, []bool{NeedValue[bool]("TEST_ARRAY_ONE_ELEMENT")}, Array("TEST_ARRAY_ONE_ELEMENT", []bool{true, true, false}))
			assert.Equal(t, []bool{true, false, true, false}, Array("TEST_ARRAY", []bool{false, true, false, true}))
		},
	)
}
