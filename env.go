package env

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"golang.org/x/exp/constraints"
)

var (
	ErrNoEnvFound      = fmt.Errorf("no env found")
	ErrWrongFormat     = fmt.Errorf("error during parse environment")
	ErrUnsupportedType = fmt.Errorf("unsupported type")
)

type Environment interface {
	constraints.Ordered | bool | time.Duration
}

func init() {
	_ = godotenv.Load()
}

func Value[T Environment](key string, fallback T) T {
	result, err := MustValue[T](key)
	if err != nil {
		result = fallback
	}

	return result
}

func Array[T Environment](key string, fallback []T) []T {
	result, err := MustArray[T](key)
	if err != nil {
		result = fallback
	}

	return result
}

func Json[T any](key string, fallback T) T {
	result, err := MustJson[T](key)
	if err != nil {
		result = fallback
	}

	return result
}

func NeedValue[T Environment](key string) T {
	v, err := MustValue[T](key)
	if err != nil {
		panic(err)
	}
	return v
}

func NeedArray[T Environment](key string) []T {
	v, err := MustArray[T](key)
	if err != nil {
		panic(err)
	}
	return v
}

func NeedJson[T any](key string) T {
	v, err := MustJson[T](key)
	if err != nil {
		panic(err)
	}
	return v
}

func MustValue[T Environment](key string) (T, error) {
	value, exists := os.LookupEnv(key)
	if !exists {
		return *new(T), fmt.Errorf("%w: %s", ErrNoEnvFound, key)
	}

	result, err := convert[T](value)
	if err != nil {
		return *new(T), fmt.Errorf("%w: %s: %w", ErrWrongFormat, key, err)
	}
	return result.(T), nil
}

func MustArray[T Environment](key string) ([]T, error) {
	value, exists := os.LookupEnv(key)
	if !exists {
		return nil, fmt.Errorf("%w: %s", ErrNoEnvFound, key)
	}

	var result []T
	re := regexp.MustCompile(" *, *")
	list := re.Split(value, -1)
	for _, element := range list {
		v, err := convert[T](element)
		if err != nil {
			return nil, fmt.Errorf("%w: %s: %w", ErrWrongFormat, key, err)
		}
		result = append(result, v.(T))
	}

	return result, nil
}

func MustJson[T any](key string) (T, error) {
	var param T
	value, exists := os.LookupEnv(key)
	if !exists {
		return param, fmt.Errorf("%w: %s", ErrNoEnvFound, key)
	}

	err := json.Unmarshal([]byte(value), &param)
	if err != nil {
		return param, fmt.Errorf("%w: %s: %w", ErrWrongFormat, key, err)
	}
	return param, nil
}

func convert[T Environment](value string) (any, error) {
	var result T
	switch any(result).(type) {
	case string:
		return value, nil
	case time.Duration:
		return time.ParseDuration(value)
	case int:
		v, err := readInt(value)
		if err == nil && int64(int(v)) != v {
			return result, fmt.Errorf("value is out of range for int")
		}
		return int(v), err
	case int64:
		v, err := readInt(value)
		return v, err
	case int32:
		v, err := readInt(value)
		if err == nil && int64(int32(v)) != v {
			return result, fmt.Errorf("value is out of range for int32")
		}
		return int32(v), err
	case int16:
		v, err := readInt(value)
		if err == nil && int64(int16(v)) != v {
			return result, fmt.Errorf("value is out of range for int16")
		}
		return int16(v), err
	case int8:
		v, err := readInt(value)
		if err == nil && int64(int8(v)) != v {
			return result, fmt.Errorf("value is out of range for int8")
		}
		return int8(v), err
	case uint:
		v, err := readUint(value)
		if err == nil && uint64(uint(v)) != v {
			return result, fmt.Errorf("value is out of range for uint")
		}
		return uint(v), err
	case uint64:
		v, err := readUint(value)
		return v, err
	case uint32:
		v, err := readUint(value)
		if err == nil && uint64(uint32(v)) != v {
			return result, fmt.Errorf("value is out of range for uint32")
		}
		return uint32(v), err
	case uint16:
		v, err := readUint(value)
		if err == nil && uint64(uint16(v)) != v {
			return result, fmt.Errorf("value is out of range for uint16")
		}
		return uint16(v), err
	case uint8:
		v, err := readUint(value)
		if err == nil && uint64(uint8(v)) != v {
			return result, fmt.Errorf("value is out of range for uint8")
		}
		return uint8(v), err
	case float64:
		v, err := readFloat(value)
		return v, err
	case float32:
		v, err := readFloat(value)
		return float32(v), err
	case bool:
		v, err := readBool(value)
		return v, err
	default:
		return result, fmt.Errorf("%w: %T", ErrUnsupportedType, result)
	}
}

func readInt(value string) (int64, error) {
	return strconv.ParseInt(value, 10, 0)
}

func readUint(value string) (uint64, error) {
	return strconv.ParseUint(value, 10, 0)
}

func readFloat(value string) (float64, error) {
	return strconv.ParseFloat(value, 64)
}

func readBool(value string) (bool, error) {
	switch value {
	case "1", "true":
		return true, nil
	case "0", "false":
		return false, nil
	default:
		return false, fmt.Errorf("non-bool expression: '%s'", value)
	}
}
